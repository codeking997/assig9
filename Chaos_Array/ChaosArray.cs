﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chaos_Array
{
 
    
    class ChaosArray<T>
    {
        public T[] myArray;

        public int size=5;

        
        public ChaosArray()
        {
            myArray = new T[5];
        }
        //this method inserts numbers into the array, the if statement checks if the random position is occupied by another element
        //if it is not then it pushes the element into the array, otherwise it throws a custom exception
        public void Insert(T num)
        {
            Random rand = new Random();
            

            int random = rand.Next(0, size);
            //rand.Next(1, size);
            if (EqualityComparer<T>.Default.Equals(myArray[random], default(T)))
            {
                
                myArray[random] = num;
            }else
            {
                throw new CustomExeption();
            }

           
           
        }
        //this method prints all the elements of the Array to the console using a foreach loop. 
        public  void Print()
        {
            
            foreach(T item in myArray)
            {
                Console.WriteLine(item);
            }
        }
        /*
         * this method retrives a random item in the array, if it picks an empty element then it throws 
         * a custom exception
        */
        public void Retrive()
        {
            Random rand1 = new Random();

            

            int random = rand1.Next(0, size);
            
            
                if (!EqualityComparer<T>.Default.Equals(myArray[random], default(T)))
                {
                    
                    Console.WriteLine(myArray[rand1.Next(0, size)]);
            }
            else
            {
                throw new CustomExeption();
            }
            
            
           
            
                
            
           
           
            
        }
    }
}
